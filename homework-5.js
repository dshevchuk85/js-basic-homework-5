
function createNewUser() {
  let firstName = prompt('Ваше имя?');
  let lastName = prompt('Ваша Фамилия');

  const newUser = {
    firstName,
    lastName,
    getLogin() {
      return (this.firstName.charAt(0) + this.lastName).toLowerCase();
    }
  }
  return newUser;
}

const user = createNewUser();
console.log(user.getLogin());